import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from './app.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  res1 = [];
  res2 = [];
  res3 = [];
  res4r = [];
  res5r = [];

  private subscription: Subscription;
  constructor(private appService: AppService) { }

  ngOnInit() {
    this.subscription = this.appService.picture()
      .subscribe(
        data => {
          this.res1 = data.slice(0, 5);
          this.res2 = data.slice(5, 10);
          this.res3 = data.slice(10, 15);
          this.res4r = data.slice(15, 20);
          this.res5r = data.slice(20, 28);
        },
        error => alert(error)
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
