import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

const ApiUrl = 'https://picsum.photos/v2/list?page=2&limit=100';
const headerConfig = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
    providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  picture(): Observable<any> {
    return this.http.get<any>(ApiUrl, headerConfig);
  }
}
